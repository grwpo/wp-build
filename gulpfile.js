'use strict';

const gulp       = require('gulp');
const $          = require('gulp-load-plugins')({ pattern: '*' });
const requireDir = require('require-dir');
const tasks      = requireDir('theme-src/gulp/tasks');

gulp.task('serve', callback => {
    $.runSequence(
        'theme:clean',
        'theme',
        'vendor:watch',
        ['scripts:watch', 'assets:watch', 'styles:watch', 'theme:watch'],
        'init-server',
        callback
    );
});

gulp.task('build', callback => {
    $.runSequence(
        'theme:clean',
        'theme',
        'vendor',
        ['scripts', 'assets', 'styles'],
        callback
    );
});

gulp.task('default', ['build']);
