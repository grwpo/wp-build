Installation
===
- `npm install && bower install`

Development
===
- `gulp serve` to build, watch and start a web server
- `gulp` or `gulp build` to just build 

Production
===
- `gulp --production` or `gulp -p`
- `gulp serve --production` or `gulp serve -p` can be helpful to debug production code
