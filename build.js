/**
 * Require JS build file
 * @see {@link http://www.requirejs.org/docs/optimization.html#options} for all config options 
 * @see {@link https://github.com/requirejs/example-multipage-shim} for an example project
 */

({
    appDir:         'public/assets/js',
    dir:            'public/assets/js',
    mainConfigFile: 'public/assets/js/require.config.js',
    baseUrl:        'vendor',

    allowSourceOverwrites: true,
    keepBuildDir:          true,
    skipDirOptimize:       true,
    optimize:              'none',

    modules: [
        {
            name: '../require.config',
            include: ['../Common']
        },
        {
            name: '../pages/index/Index',
            exclude: ['../require.config']
        }
    ]
})
