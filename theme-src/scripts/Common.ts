/// <reference path="../definitions/jquery/jquery.d.ts" />
/// <amd-dependency path="ScrollToPlugin" />

import $ = require('jquery');
import Nav from './layout/nav/Nav';

class Common {

    constructor() {
        $('html').css('background', 'blue');
        console.log('Common()');
        new Nav();
    }
}

new Common();