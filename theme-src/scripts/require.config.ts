/// <reference path="../definitions/requirejs/require.d.ts" />

require.config({
    baseUrl: './assets/js/vendor',
    deps: ['../Common'],
    shim: {
        'ScrollToPlugin': ['TweenMax']
    },
    waitSeconds: 0
});