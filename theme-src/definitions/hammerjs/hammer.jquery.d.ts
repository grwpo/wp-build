/// <reference path="hammerjs.d.ts" />

interface JQuery {
    hammer(options?: {}): JQuery;
}
