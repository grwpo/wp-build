'use strict';

const config    = require('./config.js');
const bsPackage = require('../../node_modules/browser-sync/package.json');

module.exports = {

    redirect(relativePath) {

        return `<?php`
            + `\n/**`
            + `\n * DEBUG FROM BUILD DIRECTORY IN DEV MODE.`
            + `\n */`
            + `\ninclude get_template_directory() . DIRECTORY_SEPARATOR . '${relativePath}';`;
    },

    browserSync() {

        return `\n`
            + `\n/**`
            + `\n * LIVE RELOADING SNIPPET`
            + `\n */`
            + `\nadd_action('wp_footer', function () { ?>`
            + `\n<script async src='http://localhost:${config.server.port}/browser-sync/browser-sync-client.${bsPackage.version}.js'></script>`
            + `\n<?php }, 999);`;
    },
};
