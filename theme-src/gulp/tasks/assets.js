'use strict';

const gulp   = require('gulp');
const $      = require('gulp-load-plugins')({ pattern: '*' });
const config = require('../config.js').assets;
const utils  = require('../utils.js');

// NOTE:
// Every task is separated so it watch and cleans its own folder
// and assets and not the whole assets folders and files

// Clean dest folders 
gulp.task('audios:clean', utils.clean(config.audios.dest));
gulp.task('fonts:clean', utils.clean(config.fonts.dest));
gulp.task('images:clean', utils.clean(config.images.dest));
gulp.task('videos:clean', utils.clean(config.videos.dest));

// Clean dest folders and then clone src folders/files
gulp.task('audios', ['audios:clean'], utils.clone(config.audios, true));
gulp.task('fonts', ['fonts:clean'], utils.clone(config.fonts, true));
gulp.task('videos', ['videos:clean'], utils.clone(config.videos, true));

// Watch src folder/files
gulp.task('audios:watch', ['audios'], utils.watch(config.audios.watch, 'audios'));
gulp.task('fonts:watch', ['fonts'], utils.watch(config.fonts.watch, 'fonts'));
gulp.task('images:watch', ['images'], utils.watch(config.images.watch, 'images'));
gulp.task('videos:watch', ['videos'], utils.watch(config.videos.watch, 'videos'));

/**
 * Clones images and, if production, optimize them
 */
gulp.task('images', ['images:clean'], () => {
    return gulp
        .src(config.images.src)
        .pipe($.plumber())
        .pipe($.if(utils.isProduction, $.imagemin({
            progressive: true,
            svgoPlugins: [{ removeViewBox: false }],
            use: [$.imageminPngquant()]
        })))
        .pipe(gulp.dest(config.images.dest))
        .on('end', utils.liveReload);
});

/**
 * Runs a sequence of tasks (audios, fonts, images, videos)
 */
gulp.task('assets', () =>
    $.runSequence(['audios', 'fonts', 'images', 'videos']));

/**
 * Runs a sequence of tasks (audios, fonts, images, videos)
 */
gulp.task('assets:watch', () =>
    $.runSequence(['audios:watch', 'fonts:watch', 'images:watch', 'videos:watch']));
