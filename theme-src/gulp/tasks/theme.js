'use strict';

const path      = require('path');
const gulp      = require('gulp');
const $         = require('gulp-load-plugins')({ pattern: '*' });
const config    = require('../config.js').theme;
const utils     = require('../utils.js');
const templates = require('../templates.js');

/**
 * Clean dest folder
 */
gulp.task('theme:clean', utils.clean(config.dest));

/**
 * Clones files to dest, in dev mode overwrites php files
 * with a redirection snippet and adds a browser sync snippet
 * to functions.php  
 */
gulp.task('theme', () => {

    const filters = {
        php: $.filter('**/*.php', { restore: true }),
        functions: $.filter('**/functions.php', { restore: true })
    };

    const overwritePipe = $.lazypipe()
        .pipe(() => filters.php)
        // Replace php files contents with redirect template
        .pipe($.vinylTransform, absolutePath =>
            $.mapStream((chunk, next) => {
                const relativePath = path.relative(config.dest, absolutePath);
                return next(null, templates.redirect(relativePath));
            })
        )
        // Append browserSync snippet to 'functions.php'
        .pipe(() => filters.functions)
        .pipe($.insert.append, templates.browserSync())
        // Remove filters
        .pipe(() => filters.functions.restore)
        .pipe(() => filters.php.restore);

    return gulp
        .src(config.src)
        .pipe($.plumber())
        .pipe($.if(utils.isDevelopment, overwritePipe()))
        .pipe(gulp.dest(config.dest))
        .on('end', utils.liveReload);
});

/**
 * Watch theme files
 */
gulp.task('theme:watch', ['theme'], utils.watch(config.watch, 'theme'));
