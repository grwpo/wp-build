"use strict";

const gulp   = require("gulp");
const $      = require("gulp-load-plugins")({ pattern: "*" });
const config = require("../config.js").server;
const server = $.browserSync.create();

/**
 * Init Browser Sync to listen for changes and reload server
 */
gulp.task("init-server", () => {

    server.init({
        ghostMode: false,
        logSnippet: false,
        server: { baseDir: './public' }
        //socket: { domain: `localhost:${config.port}` }
    }, () => {
        console.log(
            $.chalk.white(" Open your project in the browser or reload it") + "\n" +
            $.chalk.gray(" ---------------------------------------------")
        );
    });
});

module.exports = server;
