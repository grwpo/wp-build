'use strict';

const gulp   = require('gulp');
const $      = require('gulp-load-plugins')({ pattern: '*' });
const config = require('../config.js').vendor;
const utils  = require('../utils.js');

/**
 * Removes vendor dest files
 */
gulp.task('vendor:clean', utils.clean([config.dest]));

/**
 * Move vendor src files to dest
 */
gulp.task('vendor:clone', utils.clone(config, true));

/**
 * Removes vendor and bower dest files,
 * move bower files to vendor dest and
 * clones vendor src files to dest
 */
gulp.task('vendor', callback => {
    $.runSequence('vendor:clean', 'bower:clean', 'bower', 'vendor:clone', callback);
});

/**
 * Vendor watch
 */
gulp.task('vendor:watch', ['vendor'], utils.watch(config.watch, ['vendor']));