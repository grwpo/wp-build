'use strict';

const gulp   = require('gulp');
const $      = require('gulp-load-plugins')({ pattern: '*' });
const config = require('../config.js').bower;
const utils  = require('../utils.js');

/**
 * Removes bower files from their dest
 */
gulp.task('bower:clean', utils.clean([config.dest.css, config.dest.js]))

/**
 * Filters css and js files from the bower main files,
 * remanes css files (ej. reset.css to _reset.scss) and moves them to its dest,
 * moves the js files to its dest
 */
gulp.task('bower', () => {
    const files   = $.mainBowerFiles();
    const filters = {
        js:  $.filter('**/*.js', { restore: true }),
        css: $.filter('**/*.css', { restore: true }) };

    return gulp
        .src(files)
        .pipe($.plumber())
        .pipe(filters.css)
        .pipe($.rename({ prefix: '_', extname: '.scss' }))
        .pipe(gulp.dest(config.dest.css))
        .pipe(filters.css.restore)
        .pipe(filters.js)
        .pipe(gulp.dest(config.dest.js));
});