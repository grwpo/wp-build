'use strict';

const gulp        = require('gulp');
const $           = require('gulp-load-plugins')({ pattern: '*' });
const config      = require('../config.js').styles;
const utils       = require('../utils.js');
const browserSync = require('./serve.js');

/**
 * Compile SASS files, if development generate sourcemaps,
 * and in production minimizes css output
 */
gulp.task('styles', () => {

    return gulp
        .src(config.src)
        .pipe($.plumber())
        .pipe($.if(utils.isDevelopment, $.sourcemaps.init()))
        .pipe($.sass({
            includePaths: $.nodeBourbon.includePaths,
            outputStyle: utils.isDevelopment ? 'nested' : 'compressed'
        }))
        // Log error with SASS: https://github.com/floatdrop/gulp-plumber/issues/32
        .on('error', $.sass.logError)
        .pipe($.if(browserSync.active, browserSync.stream()))
        .pipe(gulp.dest(config.dest))
        // To avoid reload write maps after browserSync stream 
        .pipe($.if(utils.isDevelopment, $.sourcemaps.write('./maps')))
        .pipe(gulp.dest(config.dest));
});

/**
 * Watch styles source
 */
gulp.task('styles:watch', ['styles'], utils.watch(config.watch, 'styles'));
