'use strict';

const path   = require('path');
const gulp   = require('gulp');
const $      = require('gulp-load-plugins')({ pattern: '*' });
const config = require('../config.js').scripts;
const utils  = require('../utils.js');

/**
 * Typescript Project
 * @see {@link https://www.npmjs.com/package/gulp-typescript#incremental-compilation}
 * @see {@link https://www.npmjs.com/package/gulp-typescript#resolving-files} 
 */
const tsProject = $.typescript.createProject({
    target: 'ES5',
    module: 'amd',
    noExternalResolve: true
});

/**
 * Compiles `.ts` files and, if development,
 * generates sourcemaps
 */
gulp.task('typescript', () => {

    const output = gulp 
        .src(config.src)
        .pipe($.if(utils.isDevelopment, $.sourcemaps.init()))
        .pipe($.typescript(tsProject)); 

    return output.js
        .pipe($.if(utils.isDevelopment, $.sourcemaps.write('./maps')))
        .pipe(gulp.dest(config.dest))
        .on('end', utils.liveReload);
});

/**
 * Executes the requirejs optimization tool command,
 * the rjs configuration file 'build.js' must be along
 * with the 'gulpfile.js' file
 * @see {@link http://www.requirejs.org/docs/optimization.html#basics}
 * @see {@link https://www.npmjs.com/package/gulp-run}
 */
gulp.task('rjs', () => {

    return $.run('r.js -o build.js').exec()
        .pipe($.plumber())
        .pipe($.rename('build.txt'))
        .pipe(gulp.dest(config.dest))
        .on('end', utils.liveReload);
});

/**
 * Compiles typescript and, if production, resolves
 * modules with the requirejs optimization tool
 */
gulp.task('scripts', (callback) => {

    if (utils.isProduction) $.runSequence('typescript', 'rjs', callback);
    else $.runSequence('typescript', callback);
});

/**
 * Watches `.ts` files
 */
gulp.task('scripts:watch', ['scripts'], utils.watch(config.watch, ['scripts']));
