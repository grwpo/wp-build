'use strict';

// INDEX
// -------------------------------
// 1.$PROJECT
// 2.$THEME
// 3.$ASSETS
// 4.$STYLES
// 5.$SCRIPTS
// 6.$BOWER
// 7.$VENDOR
// 8.$SERVER
// -------------------------------

module.exports = (function () {
    const utils = require('./utils.js');

    // 1.$PROJECT
    // ----------
    const project = {
        src:  './theme-src',
        dest: './public'
    };

    // 2.$THEME
    // --------
    const theme = {
        src:   `${project.src}/theme/**/*`,
        dest:  project.dest,
        watch: `${project.src}/theme/**/*.{css,hbs,html,jade,js,json,lock,php,png}`
    };

    // 3.$ASSETS
    // ---------
    const assets = {
        src:  `${project.src}/assets`,
        dest: `${project.dest}/assets`
    };

    assets.audios = {
        src:   `${assets.src}/audios/**/*.{mp3,ogg,ogm,ogv,wav}`,
        dest:  `${assets.dest}/audios`,
        watch: `${assets.src}/audios/**/*.{mp3,ogg,ogm,ogv,wav}`
    };

    assets.fonts = {
        src:   `${assets.src}/fonts/**/*.{eot,svg,ttf,woff,woff2}`,
        dest:  `${assets.dest}/fonts`,
        watch: `${assets.src}/fonts/**/*.{eot,svg,ttf,woff,woff2}`
    };

    assets.images = {
        src:   `${assets.src}/images/**/*.{gif,ico,jpeg,jpg,png,svg,webp}`,
        dest:  `${assets.dest}/images`,
        watch: `${assets.src}/images/**/*.{gif,ico,jpeg,jpg,png,svg,webp}`
    };

    assets.videos = {
        src:   `${assets.src}/videos/**/*.{mp4,ogg,ogm,ogv,webm}`,
        dest:  `${assets.dest}/videos`,
        watch: `${assets.src}/videos/**/*.{mp4,ogg,ogm,ogv,webm}`
    };

    // 4.$STYLES
    // ---------
    const styles = {
        src:   `${project.src}/styles/**/*.{sass,scss}`,
        dest:  `${assets.dest}/css`,
        watch: `${project.src}/styles/**/*.{sass,scss}`,
    };

    // 5.$SCRIPTS
    // ----------
    const scripts = {
        src:  [
            `${project.src}/scripts/**/*.ts`,
            `${project.src}/definitions/**/*.ts`
        ],
        dest: `${assets.dest}/js`,
        watch: [
            `./build.js`,
            `${project.src}/scripts/**/*.ts`,
            `${project.src}/definitions/**/*.ts`,
        ],
    };

    // 6.$BOWER
    // --------
    const bower = {
        dest: {
            js:  `${scripts.dest}/vendor`,
            css: `${utils.globParent(styles.src)}/bower` }
    };

    // 7.$VENDOR
    // ---------
    const vendor = {
        src:   `${assets.src}/js/vendor/**/*.js`,
        dest:  `${assets.dest}/js/vendor`,
        watch: [
            `./bower.json`,
            `${assets.src}/js/vendor/**/*.js`]
    };

    // 8.$SERVER
    // ---------
    const server = {
        port: 3000
    };


    return {
        project: project,
        theme:   theme,
        assets:  assets,
        styles:  styles,
        scripts: scripts,
        bower:   bower,
        vendor:  vendor,
        server:  server
    }
})();
