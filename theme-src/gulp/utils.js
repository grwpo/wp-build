'use strict';

const path        = require('path');
const gulp        = require('gulp');
const $           = require('gulp-load-plugins')({ pattern: '*' });
const browserSync = require('./tasks/serve.js');

module.exports = {

    /**
     * Converts a string to and Array
     * @param {string|string[]} target
     * @returns {[]}
     */
    strToArr(target) {
        const isArr = Array.isArray(target);
        if (!isArr) return typeof target === 'string' ? [target] : [];
        return isArr ? target : [];
    },

    /**
     * Get if production argument was passed
     * @type {boolean}
     */
    get isProduction() {
        const argv = $.minimist(process.argv.slice(2));
        return argv.p || argv.production;
    },

    /**
     * Get if production argument wasn't passed
     * @type {boolean}
     */
    get isDevelopment() {
        return !this.isProduction
    },

    /**
     * Gulp task that deletes folder/files
     * @param {string|string[]} path
     * @returns {Function}
     */
    clean(path) {
        return () => gulp
            .src(path)
            .pipe($.plumber())
            .pipe($.vinylPaths(paths => {
                return $.del(paths);
            }));
    },

    /**
     * Gulp task that clones folder/files
     * @param {Object} paths
     * @param {string|string[]} paths.src
     * @param {string|string[]} paths.dest
     * @param {boolean} reload - Reload browser when done
     * @returns {Function}
     */
    clone(paths, reload) {

        return () => gulp
            .src(paths.src)
            .pipe(gulp.dest(paths.dest))
            .on('end', this.liveReload);
    },

    /**
     * Gulp task that watch the given array of paths
     * @param {string|string[]} paths
     * @param {string|string[]} sequence
     * @returns {Function}
     */
    watch(paths, sequence) {
        // Normalize inputs.
        paths = this.strToArr(paths);
        sequence = this.strToArr(sequence);

        return () => {
            $.watch(paths, () => $.runSequence.apply(this, sequence));
        };
    },

    /**
     * Reloads browser if browserSync has been initialized
     */
    liveReload() {
        if (browserSync.active) {
            browserSync.reload();
        }
    },

    /**
     * Test for glob or extglob pattern
     * @param {string} str
     * @returns {boolean}
     * @see {@link https://github.com/jonschlinkert/is-glob}
     * @see {@link https://github.com/jonschlinkert/is-extglob}
     * @author jonschlinkert
     */
    isGlob(str) {
        return typeof str === 'string' &&
            (/[*!?{}(|)[\]]/.test(str) ||
            /[@?!+*]\(/.test(str));
    },

    /**
     * Extract the non-magic parent path from a glob string
     * @param {string} str
     * @returns {string}
     * @see {https://github.com/es128/glob-parent/blob/master/index.js}
     * @author es128
     */
    globParent(str) {
        str += 'a'; // preserves full path in case of trailing path separator
        do { str = path.dirname(str) } while (this.isGlob(str));
        return str;
    }
};
